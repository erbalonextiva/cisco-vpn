.PHONY: all

all: connect

connect: verify
	@echo "Attempting to connect with Cisco VPN" 
	./main.sh

off:
	@echo "Logging off"
	./disconnect_vpn.sh

verify: grant
	./expect_verify.sh

grant:
	@echo "Giving permissions..."
	chmod +x main.sh
	chmod +x expect_verify.sh
	chmod +x start_vpn.exp
	chmod +x disconnect_vpn.sh
