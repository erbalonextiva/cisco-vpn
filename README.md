# Cisco VPN with DUO

This small script aims to facilitate access to a vpn, which has DUO two-step authentication in command line.

## Why?

Because the GUI from Cisco AnnyConnect client uses nearly 1GB from Memory and I always prefer run VPNs in background

![picture](https://bitbucket.org/erbalonextiva/cisco-vpn/raw/4190efa54fa2f22ff7cbe0434ef796fa21b3213a/CIsco%20Memory%20Usage.png)

## Getting started

### Packages that you need

###### *1. Cisco AnyConnect Secure Mobility Client*

**Visit:** this [page](https://www.cisco.com/c/en/us/support/security/anyconnect-secure-mobility-client-v4-x/model.html#~tab-downloads "page")

###### *2. Expect*
`$ sudo apt-get install expect` or  `$ sudo yum install expect`

**Note:** For OSX you can try with HomeBrew

`$ brew install expect`

### Configuration

The script was designed to read the vpn.properties, just fill the correct information on each key.

Example

```
vpn.user=pepe
vpn.password=pepe1234
vpn.host=sie.domain.com
vpn.group=0
```

## Execution

To execute the script, you should use make command

To connect:

`$ make`

After typing the previous command, a notification is going to be sent to you in your DUO application.

**Note:** In some cases you need to open your DUO application to see the request.

To disconnect:

`$ make off`

### Possible issues

The scripts should be in same group with the login user but depending for your distro sometimes each script could has an unknown group.

You can fix with this command:

`chown user:user file` and `chmod +x file`

Example

`chown pepe:pepe *.sh` and  `chmod +x *.sh`

