#!/bin/bash

EXPECT_EXEC="/usr/local/bin/expect"
TO_FIND="which expect"

FOUNDED=$(eval $TO_FIND)

if [ -e "$EXPECT_EXEC" ]
then
	echo "$EXPECT_EXEC founded in bin directory."
elif [ -e "$FOUNDED" ]
then
    echo "$FOUNDED found."
else
    echo "expect not found, please try to install."
    echo "Using brew -> brew install expect"
    echo "or manually -> http://www.linuxfromscratch.org/blfs/view/svn/general/expect.html"
    exit 1
fi
