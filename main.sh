#!/bin/bash

##### GLOBAL VARIABLES #####
CN_ADDR="domain"
CN_USER="$USER"
CN_PWD="default"
CN_GROUP="6"
CN_PROP=vpn.properties

function pause() {
    read -p "$*"
}

function printUser() {
    echo -e ">>>>> Login as user[$CN_USER]"
}

function printParams(){
    echo "$CN_ADDR $CN_USER $CN_PWD $CN_GROUP"
}

function connect(){
    printUser
    printParams
    expect ./start_vpn.exp $CN_ADDR $CN_USER $CN_PWD $CN_GROUP
}

function getProperty() {
    PROP_KEY=$1
    PROP_VALUE=`cat $CN_PROP | grep "$PROP_KEY" | cut -d'=' -f2`
    echo $PROP_VALUE
}

function updateProperty() {
    replaceString=$1
    sed -i "s/\(example\.java\.property=\).*\$/\1${replaceString}/" $CN_PROP
}

function validateRoot() {
    # Make sure only root can run our script
    if [[ $EUID -ne 0 ]]; then
       echo -e ">>>> This script must be run as sudo" 1>&2
       exit 1
    fi
}

function readProperties() {
    echo -e "# Reading properties.... Just wait..."
    CN_ADDR=$(getProperty "vpn.host")
    CN_USER=$(getProperty "vpn.user")
    CN_PWD=$(getProperty "vpn.password")
    CN_GROUP=$(getProperty "vpn.group")
}

##### INIT #####
readProperties
connect
